# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.17)
# Base de datos: gabinetes
# Tiempo de Generación: 2018-03-27 01:23:45 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla candidato
# ------------------------------------------------------------

DROP TABLE IF EXISTS `candidato`;

CREATE TABLE `candidato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidatura_id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `propuestas` longtext COLLATE utf8_unicode_ci NOT NULL,
  `compromiso` tinyint(1) NOT NULL,
  `publico` tinyint(1) NOT NULL,
  `carta` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2867675A397082D4` (`candidatura_id`),
  CONSTRAINT `FK_2867675A397082D4` FOREIGN KEY (`candidatura_id`) REFERENCES `candidatura` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `candidato` WRITE;
/*!40000 ALTER TABLE `candidato` DISABLE KEYS */;

INSERT INTO `candidato` (`id`, `candidatura_id`, `nombre`, `foto`, `propuestas`, `compromiso`, `publico`, `carta`)
VALUES
	(1,1,'Roberto Albores Gleason','guilson.jpg','[{\"propuesta\": \"gobernar\"}]',0,1,''),
	(2,2,'LUIS FERNANDO CASTELLANOS CAL Y MAYOR','cali.jpg','[{\"propuesta\": \"ser alcalde\"}]',0,1,''),
	(3,6,'Roberto Albores Gleason','guilson.jpg','[{\"propuesta\": \"gobernar\"}]',0,1,''),
	(4,9,'Roberto Albores Gleason','guilson.jpg','[{\"propuesta\": \"gobernar\"}]',1,1,'IMG_2170.JPG'),
	(7,11,'LUIS FERNANDO CASTELLANOS CAL Y MAYOR','cali.jpg','[{\"propuesta\": \"ser alcalde\"}]',0,1,'');

/*!40000 ALTER TABLE `candidato` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla candidatura
# ------------------------------------------------------------

DROP TABLE IF EXISTS `candidatura`;

CREATE TABLE `candidatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo` int(11) NOT NULL,
  `cve_municipio` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `id_postulante` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `candidatura_unique` (`id_tipo`,`cve_municipio`,`id_postulante`),
  KEY `IDX_E45617A1FB0D0145` (`id_tipo`),
  KEY `IDX_E45617A1637D2F7` (`cve_municipio`),
  KEY `IDX_E45617A1B91E1850` (`id_postulante`),
  CONSTRAINT `FK_E45617A1637D2F7` FOREIGN KEY (`cve_municipio`) REFERENCES `municipio` (`cve_mun`),
  CONSTRAINT `FK_E45617A1B91E1850` FOREIGN KEY (`id_postulante`) REFERENCES `postulante` (`id`),
  CONSTRAINT `FK_E45617A1FB0D0145` FOREIGN KEY (`id_tipo`) REFERENCES `tipo_condidatura` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `candidatura` WRITE;
/*!40000 ALTER TABLE `candidatura` DISABLE KEYS */;

INSERT INTO `candidatura` (`id`, `id_tipo`, `cve_municipio`, `id_postulante`)
VALUES
	(1,1,'001',1),
	(6,1,'001',2),
	(9,1,'001',3),
	(11,1,'001',5),
	(2,2,'102',1),
	(4,2,'102',2);

/*!40000 ALTER TABLE `candidatura` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla migration_versions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration_versions`;

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;

INSERT INTO `migration_versions` (`version`)
VALUES
	('20180220155010'),
	('20180220173925'),
	('20180220180415');

/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla municipio
# ------------------------------------------------------------

DROP TABLE IF EXISTS `municipio`;

CREATE TABLE `municipio` (
  `cve_mun` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cve_mun`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `municipio` WRITE;
/*!40000 ALTER TABLE `municipio` DISABLE KEYS */;

INSERT INTO `municipio` (`cve_mun`, `nombre`)
VALUES
	('001','Acacoyagua'),
	('002','Acala'),
	('003','Acapetahua'),
	('004','Altamirano'),
	('005','Amatán'),
	('006','Amatenango de la Frontera'),
	('007','Amatenango del Valle'),
	('008','Ángel Albino Corzo'),
	('009','Arriaga'),
	('010','Bejucal de Ocampo'),
	('011','Bella vista'),
	('012','Berriozabal'),
	('013','Bochil'),
	('014','El Bosque'),
	('015','Cacahoatán'),
	('016','Catazajá'),
	('017','Cintalapa'),
	('018','Coapilla'),
	('019','Comitán de Domínguez'),
	('020','La Concordia'),
	('021','Copainalá'),
	('022','Chalchihuitán'),
	('023','Chamula'),
	('024','Chanal'),
	('025','Chapultenango'),
	('026','Chenalhó'),
	('027','Chiapa de Corzo'),
	('028','Chiapilla'),
	('029','Chicoasén'),
	('030','Chicomuselo'),
	('031','Chilón'),
	('032','Escuintla'),
	('033','Francisco León'),
	('034','Frontera Comalapa'),
	('035','Frontera Hidalgo'),
	('036','La Grandeza'),
	('037','Huehuetán'),
	('038','Huitiupán'),
	('039','Huixtán'),
	('040','Huixtla'),
	('041','La Independencia'),
	('042','Ixhuatán'),
	('043','Ixtacomitán'),
	('044','Ixtapa'),
	('045','Ixtapangajoya'),
	('046','Jiquipilas'),
	('047','Jitotol'),
	('048','Juárez'),
	('049','Larrainzar'),
	('050','La Libertad'),
	('051','Mapastepec'),
	('052','Las Margaritas'),
	('053','Mazapa de Madero'),
	('054','Mazatán'),
	('055','Metapa de Domínguez'),
	('056','Mitontic'),
	('057','Motozintla'),
	('058','Nicolás Ruiz'),
	('059','Ocosingo'),
	('060','Ocotepec'),
	('061','Ocozocoautla de Espinosa'),
	('062','Ostuacán'),
	('063','Osumacinta'),
	('064','Oxchuc'),
	('065','Palenque'),
	('066','Pantelhó'),
	('067','Pantepec'),
	('068','Pichucalco'),
	('069','Pijijiapan'),
	('070','El Porvenir'),
	('071','Pueblo Nuevo Solistahuacán'),
	('072','Rayón'),
	('073','Reforma'),
	('074','Las Rosas'),
	('075','Sabanilla'),
	('076','Salto de Agua'),
	('077','San Cristóbal de las Casas'),
	('078','San Fernando'),
	('079','San Juan Cancuc'),
	('080','San Lucas'),
	('081','Siltepec'),
	('082','Simojovel'),
	('083','Sitalá'),
	('084','Socoltenango'),
	('085','Solosuchiapa'),
	('086','Soyaló'),
	('087','Suchiapa'),
	('088','Suchiate'),
	('089','Sunuapa'),
	('090','Tapachula'),
	('091','Tapalapa'),
	('092','Tapilula'),
	('093','Tecpatán'),
	('094','Tenejapa'),
	('095','Teopisca'),
	('096','Tila'),
	('097','Tonalá'),
	('098','Totolapa'),
	('099','La Trinitaria'),
	('100','Tumbalá'),
	('101','Tuxtla Chico'),
	('102','Tuxtla Gutiérrez'),
	('103','Tuzantán'),
	('104','Tzimol'),
	('105','Unión Juárez'),
	('106','Venustiano Carranza'),
	('107','Villacomaltitlan'),
	('108','Villa Corzo'),
	('109','Villaflores'),
	('110','Yajalón'),
	('111','Zinacantán'),
	('112','Aldama'),
	('113','Benemérito de las Américas'),
	('114','Maravilla Tenejapa'),
	('115','Marqués de Comillas'),
	('116','Montecristo de Guerrero'),
	('117','San Andrés Duraznal'),
	('118','Santiago el Pinar'),
	('120','Emiliano Zapata'),
	('121','Mezcalapa'),
	('122','El Parral'),
	('123','Capitan Luis A. Vidal'),
	('124','Rincón Chamula');

/*!40000 ALTER TABLE `municipio` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla postulante
# ------------------------------------------------------------

DROP TABLE IF EXISTS `postulante`;

CREATE TABLE `postulante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `independiente` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `postulante` WRITE;
/*!40000 ALTER TABLE `postulante` DISABLE KEYS */;

INSERT INTO `postulante` (`id`, `nombre`, `logo`, `independiente`)
VALUES
	(1,'PAN','pri.png',0),
	(2,'Independiente','ci.png',1),
	(3,'Independiente','ci.png',1),
	(4,'Independiente','ci.png',1),
	(5,'Independiente','ci.png',1);

/*!40000 ALTER TABLE `postulante` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla tipo_condidatura
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tipo_condidatura`;

CREATE TABLE `tipo_condidatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `municipios` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `tipo_condidatura` WRITE;
/*!40000 ALTER TABLE `tipo_condidatura` DISABLE KEYS */;

INSERT INTO `tipo_condidatura` (`id`, `nombre`, `municipios`)
VALUES
	(1,'Gobernatura del Estado',0),
	(2,'Presidencias municipales',1);

/*!40000 ALTER TABLE `tipo_condidatura` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
