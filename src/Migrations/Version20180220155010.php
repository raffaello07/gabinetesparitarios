<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180220155010 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE municipio (cve_mun VARCHAR(3) NOT NULL, nombre VARCHAR(100) NOT NULL, PRIMARY KEY(cve_mun)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE candidato (id INT AUTO_INCREMENT NOT NULL, candidatura_id INT NOT NULL, nombre VARCHAR(100) NOT NULL, foto VARCHAR(100) NOT NULL, propuestas LONGTEXT NOT NULL, compromiso TINYINT(1) NOT NULL, publico TINYINT(1) NOT NULL, INDEX IDX_2867675A397082D4 (candidatura_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE candidatura (id INT AUTO_INCREMENT NOT NULL, id_tipo INT NOT NULL, cve_municipio VARCHAR(3) NOT NULL, id_postulante INT NOT NULL, INDEX IDX_E45617A1FB0D0145 (id_tipo), INDEX IDX_E45617A1637D2F7 (cve_municipio), INDEX IDX_E45617A1B91E1850 (id_postulante), UNIQUE INDEX candidatura_unique (id_tipo, cve_municipio, id_postulante), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE postulante (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(100) NOT NULL, logo VARCHAR(100) NOT NULL, independiente TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tipo_condidatura (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(100) NOT NULL, municipios TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE candidato ADD CONSTRAINT FK_2867675A397082D4 FOREIGN KEY (candidatura_id) REFERENCES candidatura (id)');
        $this->addSql('ALTER TABLE candidatura ADD CONSTRAINT FK_E45617A1FB0D0145 FOREIGN KEY (id_tipo) REFERENCES tipo_condidatura (id)');
        $this->addSql('ALTER TABLE candidatura ADD CONSTRAINT FK_E45617A1637D2F7 FOREIGN KEY (cve_municipio) REFERENCES municipio (cve_mun)');
        $this->addSql('ALTER TABLE candidatura ADD CONSTRAINT FK_E45617A1B91E1850 FOREIGN KEY (id_postulante) REFERENCES postulante (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE candidatura DROP FOREIGN KEY FK_E45617A1637D2F7');
        $this->addSql('ALTER TABLE candidato DROP FOREIGN KEY FK_2867675A397082D4');
        $this->addSql('ALTER TABLE candidatura DROP FOREIGN KEY FK_E45617A1B91E1850');
        $this->addSql('ALTER TABLE candidatura DROP FOREIGN KEY FK_E45617A1FB0D0145');
        $this->addSql('DROP TABLE municipio');
        $this->addSql('DROP TABLE candidato');
        $this->addSql('DROP TABLE candidatura');
        $this->addSql('DROP TABLE postulante');
        $this->addSql('DROP TABLE tipo_condidatura');
    }
}
