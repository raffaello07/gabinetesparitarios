<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TipoCondidaturaRepository")
 */
class TipoCondidatura
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message = "Debe ingresar un nombre.")
     */
    private $nombre;

    /**
     * @ORM\Column(type="boolean")
     */
    private $municipios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Candidatura", mappedBy="tipo")
     */
    private $candidaturas;

    public function __construct()
    {
        $this->candidaturas = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return Collection|Candidatura[]
     */
    public function getCandidaturas()
    {
        return $this->candidaturas;
    }

    /**
     * @return mixed
     */
    public function getMunicipios()
    {
        return $this->municipios;
    }

    /**
     * @param mixed $municipios
     */
    public function setMunicipios($municipios): void
    {
        $this->municipios = $municipios;
    }


}
