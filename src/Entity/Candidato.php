<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidatoRepository")
 */
class Candidato
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message = "Debe ingresar un nombre.")
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $carta;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message = "Debe subir una foto.")
     */
    private $foto;

    /**
     * @ORM\Column(type="text")
     */
    private $propuestas;

    /**
     * @ORM\Column(type="boolean")
     */
    private $compromiso;

    /**
     * @ORM\Column(type="boolean")
     */
    private $publico;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidatura", inversedBy="candidatos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $candidatura;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param mixed $foto
     */
    public function setFoto($foto): void
    {
        $this->foto = $foto;
    }

    /**
     * @return mixed
     */
    public function getPropuestas()
    {
        return json_decode($this->propuestas, true);
    }

    /**
     * @param mixed $propuestas
     */
    public function setPropuestas($propuestas): void
    {
        $this->propuestas = json_encode($propuestas);
    }

    /**
     * @return mixed
     */
    public function getCompromiso()
    {
        return $this->compromiso;
    }

    /**
     * @param mixed $compromiso
     */
    public function setCompromiso($compromiso): void
    {
        $this->compromiso = $compromiso;
    }

    /**
     * @return mixed
     */
    public function getPublico()
    {
        return $this->publico;
    }

    /**
     * @param mixed $publico
     */
    public function setPublico($publico): void
    {
        $this->publico = $publico;
    }

    /**
     * @return mixed
     */
    public function getCandidatura(): Candidatura
    {
        return $this->candidatura;
    }

    /**
     * @param mixed $candidatura
     */
    public function setCandidatura(Candidatura $candidatura): void
    {
        $this->candidatura = $candidatura;
    }

    /**
     * @return mixed
     */
    public function getCarta()
    {
        return $this->carta;
    }

    /**
     * @param mixed $carta
     */
    public function setCarta($carta): void
    {
        $this->carta = $carta;
    }


}
