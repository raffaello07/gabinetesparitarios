<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidaturaRepository")
 * @ORM\Table(name="candidatura",uniqueConstraints={@UniqueConstraint(name="candidatura_unique",columns={"id_tipo", "cve_municipio", "id_postulante"})})
 */
class Candidatura
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoCondidatura", inversedBy="candidaturas")
     * @ORM\JoinColumn(name="id_tipo", referencedColumnName="id", nullable=false)
     */
    private $tipo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Municipio", inversedBy="candidaturas")
     * @ORM\JoinColumn(name="cve_municipio", referencedColumnName="cve_mun", nullable=false)
     */
    private $municipio;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Postulante", inversedBy="candidaturas")
     * @ORM\JoinColumn(name="id_postulante", referencedColumnName="id", nullable=false)
     */
    private $postulante;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Candidato", mappedBy="candidatura")
     */
    private $candidatos;

    public function __construct()
    {
        $this->candidatos = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTipo(): TipoCondidatura
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo(TipoCondidatura $tipo): void
    {
        $this->tipo = $tipo;
    }

    /**
     * @return mixed
     */
    public function getMunicipio(): Municipio
    {
        return $this->municipio;
    }

    /**
     * @param mixed $municipio
     */
    public function setMunicipio(Municipio $municipio): void
    {
        $this->municipio = $municipio;
    }

    /**
     * @return Collection|Candidato[]
     */
    public function getCandidatos()
    {
        return $this->candidatos;
    }

    /**
     * @return mixed
     */
    public function getPostulante(): Postulante
    {
        return $this->postulante;
    }

    /**
     * @param mixed $postulante
     */
    public function setPostulante(Postulante $postulante): void
    {
        $this->postulante = $postulante;
    }
}
