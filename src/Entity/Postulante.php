<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostulanteRepository")
 */
class Postulante
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $logo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $independiente;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Candidatura", mappedBy="postulante")
     */
    private $candidaturas;

    public function __construct()
    {
        $this->candidaturas = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     */
    public function setLogo($logo): void
    {
        $this->logo = $logo;
    }

    /**
     * @return Collection|Candidatura[]
     */
    public function getCandidaturas()
    {
        return $this->candidaturas;
    }

    /**
     * @return mixed
     */
    public function getIndependiente()
    {
        return $this->independiente;
    }

    /**
     * @param mixed $independiente
     */
    public function setIndependiente($independiente): void
    {
        $this->independiente = $independiente;
    }


}
