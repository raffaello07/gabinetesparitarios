<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MunicipioRepository")
 */
class Municipio
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="string", length=3)
     */
    private $cveMun;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message = "Debe ingresar un nombre.")
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Candidatura", mappedBy="municipio")
     */
    private $candidaturas;

    public function __construct()
    {
        $this->candidaturas = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getCveMun()
    {
        return $this->cveMun;
    }

    /**
     * @param mixed $cveMun
     */
    public function setCveMun($cveMun): void
    {
        $this->cveMun = $cveMun;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return Collection|Candidatur[]
     */
    public function getCandidaturas()
    {
        return $this->candidaturas;
    }


}
