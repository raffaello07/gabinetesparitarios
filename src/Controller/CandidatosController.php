<?php
/**
 * Created by PhpStorm.
 * User: raffaello
 * Date: 19/2/18
 * Time: 17:49
 */

namespace App\Controller;

use App\Entity\Candidato;
use App\Entity\Candidatura;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class CandidatosController extends Controller
{
    /**
     * @Route("/api/candidato", name="candidato_create")
     * @Method("POST")
     */
    public function candidatoCreateAction(Request $request){

    }

    /**
     * @Route("/api/public/candidatos", name="candidato_public_index")
     * @Method("GET")
     */
    public function candidatoIndexAction(Request $request){
        $em =           $this->getDoctrine()->getManager();

        $jsonResponse = array();
        $tipos =   $em->getRepository("App:TipoCondidatura")->findAll();
        if($tipos){
            $municipios  = $em->getRepository("App:Municipio")->findBy(array(), array('nombre' => 'ASC'));
            if($municipios){
                foreach ($tipos as $tipo){


                    if($tipo->getMunicipios()){
                        $munarray = array();
                        foreach ($municipios as $key => $val){
                            $candarray = array();
                            $cands = $em->getRepository("App:Candidatura")->findBy(array("tipo" => $tipo, "municipio"=>$municipios[$key]));
                            if($cands){
                                foreach ($cands as $cand){
                                    $candidato = $em->getRepository("App:Candidato")->findOneBy(array("candidatura" => $cand, "publico" => true));
                                    $postulante = $cand->getPostulante();
                                    if($candidato){
                                        $candarray[]= array(
                                            "id" => $candidato->getId(),
                                            "nombre" => $candidato->getNombre(),
                                            "foto"   => $candidato->getFoto(),
                                            "propuestas" => $candidato->getPropuestas(),
                                            "carta" => $candidato->getCarta(),
                                            "compromiso" => $candidato->getCompromiso(),
                                            "partido" => array("id"=> $postulante->getId(), "nombre" => $postulante->getNombre(), "logo" => $postulante->getLogo(), "independiente" => $postulante->getIndependiente())
                                        );
                                    }
                                }
                            }
                            $munarray[] = array(
                                "cve" => $municipios[$key]->getCveMun(),
                                "nombre" => $municipios[$key]->getNombre(),
                                "candidatos" => $candarray
                            );
                        }

                        $jsonResponse[] = array(
                            "id" => $tipo->getId(),
                            "tipo" => $tipo-> getNombre(),
                            "pormun" => $tipo->getMunicipios(),
                            "municipios" => $munarray
                        );

                    }else{
                        $candidatos = array();
                        $cands = $em->getRepository("App:Candidatura")->findBy(array("tipo" => $tipo));
                        if($cands){
                            foreach ($cands as $cand){
                                $candidato = $em->getRepository("App:Candidato")->findOneBy(array("candidatura" => $cand, "publico" => true));
                                $postulante = $cand->getPostulante();
                                if($candidato){
                                    $candidatos[]= array(
                                        "id" => $candidato->getId(),
                                        "nombre" => $candidato->getNombre(),
                                        "foto"   => $candidato->getFoto(),
                                        "propuestas" => $candidato->getPropuestas(),
                                        "carta" => $candidato->getCarta(),
                                        "compromiso" => $candidato->getCompromiso(),
                                        "partido" => array("id"=> $postulante->getId(), "nombre" => $postulante->getNombre(), "logo" => $postulante->getLogo(), "independiente" => $postulante->getIndependiente())
                                    );
                                }
                            }
                        }

                        $jsonResponse[] = array(
                            "id" => $tipo->getId(),
                            "tipo" => $tipo-> getNombre(),
                            "pormun" => $tipo->getMunicipios(),
                            "candidatos" => $candidatos
                        );

                    }

                }
            }
        }

        return new Response(
            json_encode($jsonResponse),
            200,
            array('Content-Type' => 'application/json')
        );
    }

}