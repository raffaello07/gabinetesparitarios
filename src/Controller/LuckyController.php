<?php
/**
 * Created by PhpStorm.
 * User: raffaello
 * Date: 13/2/18
 * Time: 14:51
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class LuckyController extends Controller
{
    /**
     * @Route("/lucky/number", name="lucky_number")
     */
    public function numberAction(){
        $number = mt_rand(0, 100);

        //dump($this, $number);

        return $this->render('lucky/number.html.twig', array(
            "number" => $number
        ));
    }

}