import Vue from 'vue'
import App from './App.vue'

new Vue({
  el: '#candidatos',
  render: h => h(App)
})
